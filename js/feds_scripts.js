
(function ($) {
  Drupal.behaviors.yourBehaviorName = {
    attach: function (context, settings) {
      

    var  mn = $(".feds-header");
		var mns = "feds-header-scrolled";

    var  sch = $(".search");
    var schs = "search-scrolled";
    var schbtnicon = $(".search-button i");
    var schbtn = $(".search-button");

		var hdr = $('.banner-image').height();

		$(window).scroll(function() {
  			if( $(this).scrollTop() > hdr ) {
    			mn.addClass(mns);
          sch.addClass(schs);
          schbtnicon.removeClass("fa-close");
          sch.hide();

  			} else {
    			mn.removeClass(mns);
          sch.removeClass(schs);
  			}
		});



     $("#feds-header-search-button button").click(function(){
       schbtnicon.toggleClass("fa-close");
       if(sch.css('display') === 'none') {
       schbtn.attr("aria-expanded","true");
      } else {
       schbtn.attr("aria-expanded","false");
      }
       sch.toggle();

      });


      $('.fee-charts .acc-btn').click(function() {
        $('.acc-content').slideUp('normal');
         if($(this).next().is(':hidden') == true) {
           $(this).next().slideDown('normal');
          } 
      });
      $('.fee-charts .acc-content').hide();
      
      $(".fee-charts .acc-btn").keyup(function(event) {
        if (event.keyCode === 13) {
         $('.acc-content').slideUp('normal');
          if($(this).next().is(':hidden') == true) {
            $(this).next().slideDown('normal');
           } 
        }
    });


    /* milestone menu */
    $(document).ready(function(){

      /*set attribute values for milestone year menu*/
      var yrsarry = [];
      $(function() {
      $('li.milestone-year').each(function(){
        var initcurryear = $(this).text();
        curryear = $.trim(initcurryear);
        yrsarry.push(curryear);
      });
      var i;
      for (i = 0; i < yrsarry.length; ++i) {
        $('li.milestone-year').eq(i).attr( "data-year", yrsarry[i] );
        $('li.milestone-year').eq(i).attr( "id", "yr-" + ([i+1]) );
        $('.view-id-feds_milestones>.view-content>.views-row').eq(i).attr( "id", "yr-" + ([i+1]) );
        }
      });
      

      /* add buttons before and after milestone year menu */
      $(".milestone-year-menu-container").prepend("<button id='milestone-prev'>Previous milestone</button>");
      $(".milestone-year-menu-container").append("<button id='milestone-next'>Next milestone</button>");

      //var h1 = $(".view-id-feds_milestones>.view-content>.views-row-1").position();
      var h1;
      var h2 = $(".view-id-feds_milestones>.view-content>.views-row-2").position();
      var h3 = $(".view-id-feds_milestones>.view-content>.views-row-3").position();
      var curr;

      /* In viewport */
      $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();
      
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        
      
        return elementBottom > viewportTop && elementTop < viewportBottom;
      };
      $(window).on('resize scroll', function() {
        
        var lineNum = 0;
        $('.view-id-feds_milestones>.view-content>.views-row').each(function() {
          var activeColor = $(this).attr('id'); //yr-#
          
          //activate only the first item in viewport
          if ($('.views-field-field-milestone-display-year', this).isInViewport() && lineNum <= 0) {
            $('.view-id-feds_milestones>.view-content>#' + activeColor).addClass('yr-active');
            $('.view-id-feds_milestones>.view-content>#' + activeColor).css("background-color", "yellow");
            
            $('li.milestone-year#' + activeColor).addClass('yr-active');
            $('li.milestone-year#'+ activeColor).css("background-color", "yellow");
            lineNum++;

            //find active menu item
            
            //$(activeYr) = $('.view-id-feds_milestones>.view-content>.yr-active');
            //$(prevActive) = $(activeYr).prev(); 
            $("ul.milestone-item-list").find("li.yr-active").css({"color": "red", "border": "2px solid red"});
            $("li.yr-active").prev().css({"color": "green", "border": "2px solid green"});
            


            
          } else {
            $('.view-id-feds_milestones>.view-content>#' + activeColor).removeClass('yr-active');
            $('.view-id-feds_milestones>.view-content>#' + activeColor).css("background-color", "transparent");

            $('li.milestone-year#' + activeColor).removeClass('yr-active');
            $('li.milestone-year#'+ activeColor).css("background-color", "transparent");
          }
          $curr = $(".view-id-feds_milestones>.view-content>.yr-active");
          $curr = $curr.prev();
          $curr.css( "background", "#f99" );
          //$h1 = $(".view-id-feds_milestones>.view-content>.yr-active").prev().position();
          $h1 = $curr.position();
           
        });
        
       

      });

      
 /* button scroll functions */
 $('button#milestone-prev').click(function() {
  $(h1).css("background-color", "lime");
  $('html, body').animate({
    scrollTop: $h1
  }, 500);
  return false;
}); 
     

      $("button#milestone-next").click(function() {
        $(h2).css("background-color", "purple");
        $('html, body').animate({
          scrollTop: h2.top
        }, 0);
        return false;
      }); 





    });

    
    //var FOCUSABLE_SELECTORS = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]';
    //var modal = document.querySelector('.modal');
    //var main = document.querySelector('.main');

    //modal.style.display = 'none';
    //$(".open-modal").click(function(event){
 
      // show the modal
      //modal.style.display = 'flex';
      
      // Focus the first element within the modal. Make sure the element is visible and doesnt have focus disabled (tabindex=-1);
      //modal.querySelector(FOCUSABLE_SELECTORS).focus();
  
      // Trap the tab focus by disable tabbing on all elements outside of your modal.  Because the modal is a sibling of main, this is easier. Make sure to check if the element is visible, or already has a tabindex so you can restore it when you untrap.    
    
      //var focusableElements = document.querySelectorAll(FOCUSABLE_SELECTORS);
      //$(focusableElements).not(modal.querySelector(FOCUSABLE_SELECTORS)).attr("tabIndex", "-1");
  
      // Trap the screen reader focus as well with aria roles. This is much easier as our main and modal elements are siblings, otherwise you'd have to set aria-hidden on every screen reader focusable element not in the modal.
      //modal.removeAttribute('aria-hidden');
      //main.setAttribute('aria-hidden', 'true');
    //});
    //$(".close-modal").click(function(event){
      // hide the modal
    //modal.style.display = 'none';

    // Untrap the tab focus by removing tabindex=-1. You should restore previous values if an element had them.    
    //var focusableElements = main.querySelectorAll(FOCUSABLE_SELECTORS);
    //$(focusableElements).not(modal.querySelector(FOCUSABLE_SELECTORS)).removeAttr("tabIndex");
    
    // Untrap screen reader focus
    //modal.setAttribute('aria-hidden', 'true');
    //main.removeAttribute('aria-hidden');
    
    // restore focus to the triggering element
    //openModalBtn.focus();
    //});


    $(document).mouseup(function (e)
{
    var container = $(".block-responsive-menu-combined .content .tabs .tab .content");
    var targetarea = $(".block-responsive-menu-combined .content");
    var navbtn = $(".navigation-button__toggle");

    if (!container.is(e.target) // if the target of the click isn't the container...
      && targetarea.is(e.target)
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        $('#responsive-nav-check').attr('checked', false); // Unchecks it
        navbtn.attr("aria-expanded","false");
    }
});














    }
  };
})(jQuery);


