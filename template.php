<?php

/**
 * @file
 * Add meta tags to enable modile view and head.
 */

/**
 *
 */
function feds_theme_resp_preprocess_html(&$variables) {
  // Add meta tags to enable mobile view.
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=edge',
    ),
  );
  $meta_mobile_view = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1',
    ),
  );

  $rwd_path = drupal_get_path('theme', 'feds_theme') . '/css/rwd.css';
  drupal_add_css($rwd_path);
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
  drupal_add_html_head($meta_mobile_view, 'meta_mobile_view');
}

/**
 * Implements hook_css_alter().
 */
function feds_theme_resp_css_alter(&$css) {
  // uw_core_theme and uw_fdsu_theme are loaded, but we don't want their CSS.
  // Remove any CSS added by these.
  $path = drupal_get_path('theme', 'uw_core_theme') . '/';
  foreach ($css as $key => $source) {
    if (substr($key, 0, strlen($path)) == $path) {
      unset($css[$key]);
    }
  }
  $pathFeds = drupal_get_path('theme', 'feds_theme') . '/';
  foreach ($css as $key => $source) {
    if (substr($key, 0, strlen($pathFeds)) == $pathFeds) {
      unset($css[$key]);
    }
  }

  // Remove CSS added by various modules.
  $remove['search_autocomplete'][] = '/css/themes/basic-green.css';
  $remove['ctools'][] = '/css/modal.css';
  $remove['uw_nav_global_footer'][] = '/css/uw_nav_global_footer.css';
  $remove['uw_nav_site_footer'][] = '/css/uw_nav_site_footer.css';
  $remove['system'][] = '/system.base.css';
  $remove['system'][] = '/system.theme.css';
  $remove['uw_ct_daily_bulletin'][] = '/css/uw_ct_daily_bulletin.css';
  $remove['uw_ct_embedded_call_to_action'][] = '/css/uw_ct_embedded_call_to_action.css';
  $remove['uw_ct_home_page_banner'][] = '/css/banner.css';
  $remove['uw_ct_home_page_banner'][] = '/css/uw_banner_slideshow.css';
  $remove['uw_ct_service'][] = '/css/uw_ct_service.css';
  $remove['uw_ct_blog'][] = '/css/uw_ct_blog.css';
  $remove['uw_ct_news_item'][] = '/css/uw_ct_news_item.css';
  $remove['uw_ct_event'][] = '/css/uw_ct_event.css';
  $remove['uw_ct_opportunities'][] = '/css/uw_ct_opportunities.css';
  $remove['uw_ct_person_profile'][] = '/css/uw_ct_person_profile.css';
  $remove['uw_social_media_sharing'][] = '/css/uw_social_media_sharing.css';
  $remove['date'][] = '/date_api/date.css';
  $remove['date'][] = '/date_views/css/date_views.css';
  $remove['uw_ct_contact'][] = '/css/uw_ct_contact.css';
  $remove['views'][] = '/css/views.css';
  $exclude = array();
  foreach ($remove as $module => $items) {
    if (module_exists($module)) {
      $module_path = drupal_get_path('module', $module);
      foreach ($items as $exclude_item) {
        $exclude[$module_path . $exclude_item] = TRUE;
      }
    }
  }
  $css = array_diff_key($css, $exclude);
}

/**
 *
 */
function feds_theme_resp_js_alter(&$javascript) {
  global $base_path;
  global $base_url;
  if (module_exists('uw_header_search')) {
    $rwdjs_path = drupal_get_path('module', 'uw_header_search') . '/uw_header_search.js';
    drupal_add_js($rwdjs_path);
    // drupal_add_js($tabsjs_path);
    drupal_add_js(array(
      'CToolsModal' => array(
        'modalSize' => array(
          'type' => 'scale',
          'width' => 1,
          'height' => 1,
        ),
        'modalOptions' => array(
          'opacity' => .98,
          'background-color' => '#252525',
        ),
        'animation' => 'fadeIn',
        'animationSpeed' => 'slow',
        'throbberTheme' => 'CToolsThrobber',
        // Customize the AJAX throbber like so:
        // This function assumes the images are inside the module directory's "images"
        // directory:
        'throbber' => theme('image', array(
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => t('Loading...'),
          'title' => t('Loading'),
        )),
        'closeText' => '',
        'closeImage' => theme('image', array(
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => t(''),
          'title' => t(''),
        )),
      ),
    ), 'setting');
    $exclude = array(
    // 'profiles/uw_base_profile/modules/features/uw_ct_home_page_banner/js/uw_banner_random.js' => FALSE,
    // 'profiles/uw_base_profile/modules/features/uw_ct_home_page_banner/js/uw_banner_slideshow.js' => FALSE,.
    );
    $javascript = array_diff_key($javascript, $exclude);
  }
}

/**
 * Output the global search box.
 */
function feds_theme_resp_output_search_box() {
  // Stop-gap until translation is properly implemented.
  global $language;
  global $base_path;
  global $base_url;
  if ($language->language === 'fr') {
    $search = 'Recherche';
  }
  else {
    $search = 'Search';
  }
  ?>
    <div id="global-search" class="global-search">
      <form method="get" action="//uwaterloo.ca/search">
        <div class="uw-search-wrapper">
          <?php if ($base_path !== '/') : ?>
            <span id="uw-search-site-wrap" class="uw-search-site--wrap">
              <label id="uw-search-site-label" for="uw-search-site" class="element-invisible uw-search-site--label"><?php echo t('search scope'); ?></label>
              <select name="q" id="uw-search-site" class="uw-search-site" tabindex="1">
                <option value=""><?php echo t('all sites'); ?></option>
                <option value="site:<?php echo $base_url; ?>"><?php echo t('this site'); ?></option>
              </select>
            </span>
          <?php endif; ?>
          <label id="uw-search-label" for="uw-search-term" class="uw-search--label js"><?php echo $search; ?></label>
          <input id="uw-search-term" type="text" size="31" tabindex="2" accesskey="4" name="q" class="uw-search--term">
          <input id="uw-search-submit" class="chevron-submit uw-search--submit" type="submit" value="<?php echo $search; ?>" tabindex="3">
          <input type="hidden" name="client" value="default_frontend">
          <input type="hidden" name="proxystylesheet" value="default_frontend">
          <input type="hidden" name="site" value="default_collection" />
        </div>
      </form>
    </div>
<?php
}
