

<?php
/**
 * @file
 * Default theme implementation to display a region.
 *
 * Available variables:
 * - $content: The content for this region, typically blocks.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - region: The current template type, i.e., "theming hook".
 *   - region-[name]: The name of the region with underscores replaced with
 *     dashes. For example, the page_top region would have a region-page-top class.
 * - $region: The name of the region variable as defined in the theme's .info file.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 *
 * @see template_preprocess()
 * @see template_preprocess_region()
 * @see template_process()
 */
?>



 <div id="feds-header" class="feds-header">
  <div id="feds-header-content" class="feds-header--content">

      

      <!-- MENU BUTTON -->
      <div id="feds-header-nav-button" class="feds-header--buttons__nav">
        <div class="navigation-button">
          <label for="responsive-nav-check" class="navigation-button__toggle" aria-label="navigation menu" aria-controls="navigation">
            <i style="font-size: 1.4rem; padding: 0px 1.5rem 1.5rem 0px;" class="fa fa-bars"></i>
            <span class="off-screen">Menu</span>
          </label>
        </div>
      </div>


      
      
      <!-- LOGO -->
      <a class="feds-top-logo" href="<?php print base_path() ?>"> 
        <img src="<?php print base_path() . drupal_get_path('theme', 'feds_theme'); ?>/images/wusa-logo-white-on-navy-rounded.png" alt="Waterloo Undergraduate Student Association">
      </a>

    <?php if (variable_get('uw_theme_branding') == "full") { ?>
      <nav class="nav-university feds-header--nav__university">

        <label for="nav-university-state" class="element-invisible">Main Menu State</label>
        <input class="state visuallyhidden" type="checkbox" name="nav-university-state" id="nav-university-state" autocomplete="off">

        <?php
  $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu')); 
  print drupal_render($main_menu_tree);
        ?>

      <?php print $content; ?>
      </nav>
    <?php } ?>

      <!-- SEARCH BUTTON -->
      <div id="feds-header-search-button" class="feds-header--buttons__search" role="search">
        <button class="search-button" aria-expanded="false">
            <i class="fa fa-search"></i>
            <span class="off-screen">search</span>
        </button>

      <div class="search">
        <?php
        $block = module_invoke('search', 'block_view', 'search');
        print render($block); 
        ?>
      </div>
      </div>

  </div>
</div>
