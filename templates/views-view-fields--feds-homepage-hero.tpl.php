<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<h2>Homepage Hero

<?php $arr = array(); ?>
<?php foreach ($fields as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>

  <?php print $field->wrapper_prefix; ?>
    <?php print $field->label_html; ?>
  <?php print $field->wrapper_suffix; ?>

	<?php $field_content = ($field->content); ?>


	<?php preg_match( '@src="([^"]+)"@', $field_content, $matches ); ?>
	<?php $temp = array_shift( $matches ); ?>


	<?php $temp = substr($temp,5); ?>
	<?php $temp = substr($temp, 0, -1); ?>
	<?php array_push(($arr), ($temp))?>
<?php endforeach; ?>


<?php $allvalues = (array_values($arr)); ?>

<img srcset="<?php print $allvalues[3]; ?>  2000w,
      <?php print $allvalues[2]; ?>  1024w,
      <?php print $allvalues[1]; ?> 769w,
      <?php print $allvalues[0]; ?>  320w"
   sizes="100vw"
   src="<?php print $allvalues[2]; ?>"
   alt="" />
