<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<!--?php print ($view->field['field_date_and_time_1']->advanced_render($row)); ?-->
<?php $var_date = ($view->field['field_date_and_time_1']->advanced_render($row)); ?>
<?php $str_date = strval($var_date); ?>
<?php $str_date = str_ireplace(",","",$str_date); ?>
<?php $str_date = str_ireplace("-","",$str_date); ?>

<?php
  // Get the date - for rendering individual date components.
  $date = strtotime($str_date);
  $iso = date('c', $date);
  $day = date('d', $date);
  $month = date('M', $date);
  $year = date('Y', $date);
  $time = date('g:i A', $date);
?>


  <span class="date" property="dc:date" datatype="xsd:dateTime" content="<?php
// ISO 8601 format.
print render($iso);
?>">
    <span class="date-day-month">
	    <span class="event-day"><?php print render($day); ?></span>
	    <span class="event-month"><?php print render($month); ?></span>
    </span>
    <span class="time"><?php print $time; ?></span>
  </span>


