<?php

/**
 * @file
 */
?>
<div class="breakpoint"></div>

<div class="uw-site--inner">

    <div class="uw-section--inner dk-bg">
      <div id="skip" class="skip">
          <a href="#main" class="element-invisible element-focusable uw-site--element__invisible  uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
          <a href="#feds_footer" class="element-invisible element-focusable  uw-site--element__invisible  uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
      </div>
    </div>

    <div id="social" class="feds-sm-header">
       <?php print render($page['feds_social_media']); ?>
     </div>

    <header class="uw-header--global">
    <?php print render($page['global_header']); ?>
  </header>
    <!-- Alert Alert -->
    <section class="feature-alert">
          <?php print render($page['alert_notification']); ?>
    </section>



    <div id="site--offcanvas" class="uw-site--off-canvas">
    <div class="uw-section--inner">
      <?php print render($page['site_header']); ?>
      </div>
    </div>


    <section class="main-content-area">

<?php   
if (!empty($node)) :
$heroimg = field_view_field('node', $node, 'field_hero_image', array('label'=>'hidden')); 
    print render($heroimg); 
endif; 
     ?>




    <div class="uw-header--banner__alt">
      <div class="uw-section--inner">
        <?php print render($page['banner_alt']); ?>
      </div><!-- /uw-headeranner__alt -->
    </div>
    <div id="main" class="uw-site--main" role="main">
      <div class="uw-section--inner">

        <div class="uw-site--main-top">
          <div class="uw-site--banner">
              <?php print render($page['banner']); ?>
          </div>
          <div class="uw-site--messages">
              <?php print $messages; ?>
          </div>
          <div class="uw-site--help">
              <?php print render($page['help']); ?>
          </div>
          <div class="uw-site--breadcrumb">
              <?php print $breadcrumb; ?>
          </div>
          <div class="uw-site--title">
              <?php list($title, $content) = uw_fdsu_theme_separate_h1_and_content($page, $title); ?>
              <h1><?php print $title ?: $site_name; ?></h1>
          </div>
        </div>
          <!-- when logged in -->


          
          <?php if ($tabs): ?>
            <div class="node-tabs uw-site-admin--tabs"><?php print render($tabs); ?></div><?php endif; ?>
            <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php print render($page['sidebar_first']); ?>
        <?php endif; ?>


        <div class="uw-site-main--featured-top">
          <div id="featured-top" class="uw-site-featured-top">
            <?php print render($page['featured_top']); ?>
          </div><!--/featured-top-->
        </div> <!--end uw-site-main--featured-top-->


        <div class="uw-site-main--content">
          <div id="content" class="uw-site-content">
            <?php print $content; ?>
          </div><!--/content-->
        </div> <!--end uw_site-main--content-->



        <div class="uw-site-secondary--content">
            <?php print render($page['secondary_content']); ?>
        </div>

        
        <?php $sidebar = render($page['sidebar_second']); ?>
          <?php $sidebar_promo = render($page['promo']); ?>

           <?php if (isset($sidebar) || isset($sidebar_promo)) { ?>
            <?php if (($sidebar !== NULL && $sidebar !== '') || ($sidebar_promo !== NULL && $sidebar_promo !== '')) { ?>
            <aside class="uw-site-aside">
              <div id="site-sidebar-wrapper" class="uw-site-sidebar--wrapper">
                <div id="site-sidebar-nav" class="uw-site-sidebar--nav">
                  <div id="site-sidebar" class="uw-site-sidebar<?php echo ($sidebar_promo !== NULL && $sidebar_promo !== '') ? ' sticky-promo' : ''; ?>">
                    <?php if (isset($sidebar_promo)) { ?>
                      <?php if ($sidebar_promo !== NULL && $sidebar_promo !== '') { ?>
                        <div class="uw-site-sidebar--promo">
                          <?php print render($page['promo']); ?>
                        </div>
                      <?php } ?>
                    <?php } ?>
                    <div class="uw-site-sidebar--second">
                      <?php print render($page['sidebar_second']); ?>
                    </div>
                  </div>
                </div>
              </div>
            </aside>
            <?php } ?>
          <?php } ?>



      </div><!--/section inner-->
    </div><!--/site main-->



  </section>


    <?php if (!empty($page['footer_content_1']) || !empty($page['footer_content_2']) || !empty($page['footer_content_3']) ): ?>
<footer id="feds_footer" role="contentinfo">

    
    <div id="footer-contact-wrapper">

    <?php if (!empty($page['footer_content_1'])): ?>
    <div class="footer-column">
      <?php print render($page['footer_content_1']); ?>
    </div>
    <?php endif; ?>

    <?php if (!empty($page['footer_content_2'])): ?>
    <div class="footer-column">
      <?php print render($page['footer_content_2']); ?>
    </div>
    <?php endif; ?>

    <?php if (!empty($page['footer_content_3'])): ?>
    <div class="footer-column">
      <?php print render($page['footer_content_3']); ?>
    </div>
    <?php endif; ?>

    </div> 
    </footer>
    <?php endif; ?>


      <div id="footer" class="feds-footer">
       <?php print render($page['footer']); ?>
     </div>


</div><!--/site-->
<div class="ie-resize-fix"></div>

