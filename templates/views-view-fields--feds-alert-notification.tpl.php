<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<?php if ($fields['field_type']->content == 1) { ?>
  <?php $alertcolourtheme = ('bgblush'); ?>
  <?php $alerticon = ('fa-exclamation-triangle'); ?>

<?php } else if ($fields['field_type']->content == 0) {?>
  <?php if ($fields['field_colour_scheme']->content == 1) { ?>
    <?php $alertcolourtheme = ('bgltgrey'); ?>
  <?php } else if ($fields['field_colour_scheme']->content == 2) { ?>
    <?php $alertcolourtheme = ('bgblack'); ?>
  <?php } else if ($fields['field_colour_scheme']->content == 3) { ?>
    <?php $alertcolourtheme = ('bgyellow'); ?>
  <?php } else if ($fields['field_colour_scheme']->content == 4) { ?>
    <?php $alertcolourtheme = ('bgwater'); ?>
  <?php } else if ($fields['field_colour_scheme']->content == 5) { ?>
    <?php $alertcolourtheme = ('bgblush'); ?> 
  <?php } ?>
  <?php if (!empty($fields['field_bell_icon']->content)) { ?>
    <?php $alerticon = ('fa-bell'); ?>
  <?php } ?>
<?php } ?>

<div class="alert-notification-container <?php print ($alertcolourtheme) ?>">
  <?php if (!empty($alerticon)) { ?>
    <div class="alert-icon"><i class="fas <?php print($alerticon); ?>">&nbsp;</i></div>
  <?php } ?>
<div class="alert-details">
  <?php if (!empty($fields['field_display_title']->content)) { ?>
    <strong><?php print $fields['field_display_title']->content; ?></strong>
  <?php } ?>
  <?php if (!empty($fields['body']->content)) { ?>
    <?php print $fields['body']->content; ?>
  <?php } ?>
  </div>
  </div>