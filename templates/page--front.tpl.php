<?php

/**
 * @file
 */
   

?>

<div class="breakpoint"></div>
<div id="site" data-nav-visible="false" class="<?php print $classes; ?> uw-site"<?php print $attributes; ?>>

      <div class="uw-section--inner">
        <div id="skip" class="skip">
            <a href="#main" class="element-invisible element-focusable uw-site--element__invisible  uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
            <a href="#feds_footer" class="element-invisible element-focusable  uw-site--element__invisible  uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
        </div>
      </div>

    <div id="social" class="feds-sm-header">
       <?php print render($page['feds_social_media']); ?>
     </div>
  <div class="uw-site--inner">

    <header class="uw-header--global">
      <?php print render($page['global_header']); ?>
    </header>
    <!-- Alert Alert -->
    <section class="feature-alert">
          <?php print render($page['alert_notification']); ?>
    </section>

      <!-- SLIDE MENU FOR SMALL SCREEN -->
      <div id="site--offcanvas" class="uw-site--off-canvas">
        <div class="uw-section--inner">
          <?php print render($page['site_header']); ?>
        </div>
      </div>
      
      <section class="main-content-area" id="main" role="main">
        <div class="site-title sr-only">
          <h1><?php print $site_name; ?></h1>
        </div>
        
        <div class="uw-header--banner__alt hero-split">
          <?php print render($page['feds_hero']); ?>
        </div>

        <!--section class="feature-text"-->
          <!--?php print render($page['feature_text']); ?-->
        <!--/section-->

        <!--section class="main-features feds-sectiontheme-dark"-->
          <!--?php print render($page['feds_features']); ?-->
        <!--/section-->

        <section class="feature_news feds-sectiontheme-light section-inner">
          <?php print render($page['feature_news']); ?>
        </section>

        <section class="hp-offerings feds-sectiontheme-light section-inner">
          <?php print render($page['feds_offerings']); ?>
        </section>

        <section class="feds-events feds-sectiontheme-light section-inner">
          <?php if (!empty(print render($page['feds_events']))); ?> 
        </section>
        
        <div class="uw-site--main-top">
          <div class="uw-site--banner">
              <?php print render($page['banner']); ?>
          </div>
          <div class="uw-site--messages">
              <?php print $messages; ?>
          </div>
          <div class="uw-site--help">
              <?php print render($page['help']); ?>
          </div>
          <div class="uw-site--breadcrumb">
              <?php print $breadcrumb; ?>
          </div>
        </div>
        <div class="uw-site-main--content">
         <div id="content" class="uw-site-content">
         </div>
        </div>

        <div class="feature_promo">
          <?php print render($page['feds_promo_banner']); ?>
        </div>
      </section>



    <footer id="feds_footer" role="contentinfo">
      <?php if (!empty($page['footer_content_1']) || !empty($page['footer_content_2']) || !empty($page['footer_content_3']) ): ?>
        <div id="footer-contact-wrapper">

          <?php if (!empty($page['footer_content_1'])): ?>
          <div class="footer-column">
            <?php print render($page['footer_content_1']); ?>
          </div>
          <?php endif; ?>

          <?php if (!empty($page['footer_content_2'])): ?>
          <div class="footer-column">
            <?php print render($page['footer_content_2']); ?>
          </div>
          <?php endif; ?>

          <?php if (!empty($page['footer_content_3'])): ?>
          <div class="footer-column">
            <?php print render($page['footer_content_3']); ?>
          </div>
          <?php endif; ?>

        </div> 

      <?php endif; ?>
    </footer>

    <div id="footer" class="feds-footer">
      <?php print render($page['footer']); ?>
    </div>
  </div>
</div>

<div class="ie-resize-fix"></div>

