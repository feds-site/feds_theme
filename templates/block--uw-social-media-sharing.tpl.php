<?php
/**
 * @file
 * Default theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In
 *     that case the class would be 'block-user'.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="<?php
print $block_html_id;
?>" class="<?php
print $classes;
?>"<?php
print $attributes;
?>>
  <?php
print render($title_prefix);
if ($block->subject) {
  ?>
  <h2<?php
  print $title_attributes;
  ?>><?php
  print $block->subject;
  ?>This is my block</h2>
<?php
}
?>
  <?php
print render($title_suffix);
?>
  <div class="content"<?php
print $content_attributes;
?>>
<?php
// Set up the URL in the way that the email forwarder expects.
$path = current_path();
$forward_url = drupal_encode_path($path);
// Set up the URL for social media sharers.
$share_url = url($path, array('absolute' => TRUE, 'query' => array('utm_medium' => 'shareblock')));
$share_title = html_entity_decode(drupal_get_title());
$module_path = base_path() . drupal_get_path('module', 'uw_social_media_sharing');
// Set up the URL in the way that the email forwarder expects.
if (drupal_is_front_page()) {
// If we are on the front page, we want to use "<front>" so the alias for the page is not included in the URL.
   $path = '<front>';
}
$query_url = url($path, array('absolute' => TRUE, 'query' => array('utm_medium' => 'shareblock')));
if (strpos($query_url, 'uwaterloo.ca/feds') !== FALSE){
  $query_url = str_replace("wms-dev.uwaterloo.ca/feds", "feds.ca", $query_url);
  $query_url = str_replace("uwaterloo.ca/feds", "feds.ca", $query_url);
}
$share_facebook = l('<i class="fab fa-facebook-f" aria-hidden="true"></i><span class="sr-only">Share on </span><span class="sm_title">Facebook</span>',
  'https://www.facebook.com/sharer/sharer.php',
  array('html' => TRUE, 'query' => array('u' => $query_url, 't' => $share_title)));
$share_twitter = l('<i class="fab fa-twitter" aria-hidden="true"></i><span class="sr-only">Share on </span><span class="sm_title">Twitter</span>',
  'https://twitter.com/intent/tweet',
  array('html' => TRUE, 'query' => array('source' => $query_url, 'text' => $share_title . ': ' . $query_url)));
$share_reddit = l('<i class="fab fa-reddit" aria-hidden="true"></i><span class="sr-only">Share on </span><span class="sm_title">Reddit</span>',
  'http://www.reddit.com/submit',
  array('html' => TRUE, 'query' => array('url' => $query_url, 'title' => $share_title)));
$share_linkedin = l('<i class="fab fa-linkedin-in" aria-hidden="true"></i><span class="sr-only">Share on </span><span class="sm_title">LinkedIn</span>',
  'http://www.linkedin.com/shareArticle',
  array('html' => TRUE, 'query' => array('url' => $query_url, 'title' => $share_title, 'source' => $query_url)));
$share_email = l('<i class="fas fa-envelope" aria-hidden="true"></i><span class="sr-only">Share in an </span><span class="sm_title">Email</span>',
  'forward',
  array('html' => TRUE, 'query' => array('path' => $forward_url)));
?>
<div class="feds_social_media">
<label>Share</label>
<ul class="share-buttons">
  <li><?php print $share_facebook; ?></li>
  <li><?php print $share_twitter; ?></li>
  <li><?php print $share_reddit; ?></li>
  <li><?php print $share_linkedin; ?></li>
  <li><?php print $share_email; ?></li>
</ul> 
</div> 
  
  </div>
</div>