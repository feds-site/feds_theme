<?php
/**
 * @file
 */
?>
<!DOCTYPE html><?php global $base_path; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"
  <?php print $rdf_namespaces; ?>>
<head>
  <?php print $head; ?>
  <?php $base_path = drupal_get_path('theme', 'feds_theme');?>
  <link rel="shortcut icon" href="<?php print $base_path; ?>/favicon.ico" type="image/vnd.microsoft.icon" /> 
  <title><?php print $head_title; ?></title>
  <meta name="msapplication-navbutton-color" content="#000000" />
  <meta name="msapplication-TileColor" content="#000000"/>
  <meta name="msapplication-square70x70logo" content="<?php print $base_path; ?>/images/wusa-logo-tile-70.png"/>
  <meta name="msapplication-square150x150logo" content="<?php print $base_path; ?>/images/wusa-logo-tile-150.png"/>
  <meta name="msapplication-wide310x150logo" content="<?php print $base_path; ?>/images/wusa-logo-tile-310x150.png"/>
  <meta name="msapplication-square310x310logo" content="<?php print $base_path; ?>/images/wusa-logo-tile-310.png"/>
  <meta property="og:type" content="website" />
  <meta property="og:title" content="Waterloo Undergraduate Student Association (WUSA)" />
  <meta property="og:description" content="Waterloo Undergraduate Student Association (WUSA) is the official collective voice of undergraduate students at the University of Waterloo. Since 1967, our student-led organization has been committed to improving the lives of UWaterloo undergrads, by empowering them to effect change, while working on their behalf to build a stronger campus community." />
  <meta property="og:url" content="http://www.wusa.ca" />
  <meta property="og:image" content="https://wusa.ca/sites/ca.feds/themes/feds_theme/images/wusa-logo-152.png" />
  <meta property="og:site_name" content="Waterloo Undergraduate Student Association (WUSA) | University of Waterloo" />
  <link rel="apple-touch-icon" href="<?php print $base_path; ?>/images/wusa-logo-57.png" sizes="57x57">
  <link rel="apple-touch-icon" href="<?php print $base_path; ?>/images/wusa-logo-72.png" sizes="72x72">
  <link rel="apple-touch-icon" href="<?php print $base_path; ?>/images/wusa-logo-76.png" sizes="76x76">
  <link rel="apple-touch-icon" href="<?php print $base_path; ?>/images/wusa-logo-114.png" sizes="114x114">
  <link rel="apple-touch-icon" href="<?php print $base_path; ?>/images/wusa-logo-120.png" sizes="120x120">
  <link rel="apple-touch-icon" href="<?php print $base_path; ?>/images/wusa-logo-144.png" sizes="144x144">
  <link rel="apple-touch-icon" href="<?php print $base_path; ?>/images/wusa-logo-152.png" sizes="152x152">
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!--[if lte IE 7]><script src="<?php print $base_path ?><?php print drupal_get_path('theme', 'feds_theme') ?>/scripts/iconfont.lte-ie7.js"></script><![endif]-->
  <!--[if IE 7]><link rel="stylesheet" type="text/css" href="<?php print $base_path ?><?php print drupal_get_path('theme', 'feds_theme') ?>/css/ie7.css"><![endif]-->
  <?php
  // Create empty array to hold any necessary Google Analytics codes.
  $codes = [];
  // Determine if Google Tag Manager is enabled.
  $tag_manager = module_exists('google_tag');
  // If Google Tag Manager is off, we add the global code if it's enabled.
  if (!$tag_manager && variable_get('google_analytics_enable') == 1) {
    $codes[] = 'UA-148355027-1';
  }
  // If a client-specified code is entered, we add that code.
  if (variable_get('uw_cfg_google_analytics_account')) {
    $codes[] = variable_get('uw_cfg_google_analytics_account');
  }
  // If there are any codes, we add Google Analytics.
  if ($codes) {
    ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      <?php
      foreach ($codes as $id => $code) {
        echo "ga('create', '" . $code . "', 'auto', {'name': 'tracker" . $id . "'});\n";
        echo "ga('tracker" . $id . ".send', 'pageview');\n";
      }
      ?>
    </script>
    <?php
  }


  ?>
  </head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
<!--[if lt IE 7]><div id="ie6message">Your version of Internet Explorer web browser is insecure, not supported by Microsoft, and does not work with this web site. Please use one of these links to upgrade to a modern web browser: <a href="http://www.mozilla.org/firefox/">Firefox</a>, <a href="http://www.google.com/chrome">Google Chrome</a>, <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home">Internet Explorer</a>.</div><![endif]-->
  <!--?php print $page_top; ?-->
  <?php print $page; ?>
  <!--?php print $page_bottom; ?-->
</body>
</html>